const express = require('express');
const path =require('path');
const exphbs =require('express-handlebars');
const session =require('express-session');
const methodOverride  = require("method-override");


//Initializations
const app=express();
require('./database');

//Settings
app.set('port', process.env.PORT || 8080);
app.set('views', path.join(__dirname,'views'));
app.engine('.hbs',exphbs({
    defaulLayout:'main' ,
    layourDir:path.join(app.get('views'), 'layouts') ,
    partialsDir:path.join(app.get('views'), 'partials') ,
    extName: '.hbs'
}));
app.set('view engine', '.hbs');
//Middlewares
app.set(express.urlencoded({extended:false}));
app.use(methodOverride('method'));
app.use(session({
    secret:'mysecretapp',
    resave:true,
    saveUninitialized:true
}));

//Global Variables


//Routes
app.use(require('./routes/index'));


//Static Files
app.use(express.static(path.join(__dirname,'public')));
//Server Listening

app.listen(app.get('port'), function () {
  console.log('Example app listening on port', app.get('port'));
});
