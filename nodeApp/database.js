const path = require ('path');
require('dotenv').config();
const mongoose=require('mongoose');

let uri = 'mongodb://'+ process.env.USERNAME + ':'+ process.env.PASSWORD+'@db_mongo:27017/Database?authSource=admin';

mongoose.connect(uri, {
    useCreateIndex:true,
    useNewUrlParser:true,
    useFindAndModify:false,
    useUnifiedTopology: true
})
.then(db=>console.log('DB is connected'))
.catch(err=>console.error(err));
