# Basic NodeMongoApp in Docker with Volumes

This code is useful for deploy a very basic node app with Docker and MongoDB. It makes use of some basic archives such as  package.json,index.js and database.js in order to start your development in minutes.  

Basic use of the files

1. Create a local folder when you are going to have your folder
2. Download this folder of GitLab
3. Use the command docker-compose build
4. Use the command docker-compose up
5. Your basic server of node in Docker is ready
